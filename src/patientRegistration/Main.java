package patientRegistration;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Patient patient1 = new Patient();
        Patient patient2 = new Patient();
        List<Patient> list = new ArrayList<>();
        list.add(patient1);
        list.add(patient2);
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream("Patients.txt"))) {
            for (Patient p : list) {
                dos.writeUTF(p.getName());
                dos.writeUTF(p.getSurname());
                dos.writeUTF(p.getDateBirthday().toString());
                dos.writeBoolean(p.isIllness());
            }
            System.out.println("File has been written");
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        try (DataInputStream dos = new DataInputStream(new FileInputStream("Patients.txt"))) {
            for (Patient p : list) {
                String name = dos.readUTF();
                String surname = dos.readUTF();
                String birthday = dos.readUTF();
                Boolean illness = dos.readBoolean();
                System.out.println(name + ";" + surname + ";" + birthday + ";" + illness);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}