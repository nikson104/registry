package patientRegistration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Patient {
    public String name;
    public String surname;
    public Date dateBirthday;
    public boolean illness;
    private Scanner scanner = new Scanner(System.in);

    public String getName() {
        System.out.print("Enter name of patient: ");
        name = scanner.nextLine();
        return name;
    }

    public String getSurname() {
        System.out.print("Enter surname of patient: ");
        surname = scanner.nextLine();
        return surname;
    }

    public Date getDateBirthday() {
        SimpleDateFormat date = new SimpleDateFormat("dd.MM.yyyy");
        System.out.print("Enter date of birthday: ");
        String str = scanner.nextLine();

        try {
            dateBirthday = date.parse(str);
            date = new SimpleDateFormat("dd.MM.yyyy");
            date.format(dateBirthday);
        } catch (ParseException e) {
            System.out.println("Parse Exception");
        }
        return dateBirthday;
    }

    public boolean isIllness() {
        System.out.print("The patient is Illness: ");
        illness = scanner.nextBoolean();
        return illness;
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        out.append("Name ").append(name).append(";");
        out.append(" Surname ").append(surname).append(";");
        out.append(" Date of Birthday ").append(dateBirthday).append(";");
        out.append(" illness ").append(illness);
        return out.toString();
    }
}